describe docker_container('failmap-worker-scanner') do
    it { should exist }
    it { should be_running }
end

describe docker_container('failmap-worker-storage') do
    it { should exist }
    it { should be_running }
end

describe docker_container('failmap-scheduler') do
    it { should exist }
    it { should be_running }
end

