# test failmap frontend application
describe docker_container('failmap-frontend') do
  it { should exist }
  it { should be_running }
end

# HTTP request to frontend
describe command('curl -sSvk --http1.1 https://faalkaart.test') do
  # should return successful
  its(:stderr) {should match ('HTTP/1.1 200 OK')}
  # render complete page
  its(:stdout) {should match ('MSPAINT')}
  # HSTS security should be enabled
  its(:stderr) {should match ('Strict-Transport-Security: max-age=31536000; includeSubdomains')}
end

# same for IPv6
describe command('curl -6 -sSvk --http1.1 https://faalkaart.test') do
  # should return successful
  its(:stderr) {should match ('HTTP/1.1 200 OK')}
  # render complete page
  its(:stdout) {should match ('MSPAINT')}
  # HSTS security should be enabled
  its(:stderr) {should match ('Strict-Transport-Security: max-age=31536000; includeSubdomains')}
end

# test some API calls
describe command('curl -sSvk --http1.1 "https://faalkaart.test/data/stats/NL/municipality/0"') do
  its(:stderr) {should match ('HTTP/1.1 200 OK')}
end

# test maptile proxy
describe command('curl -sSvk --http1.1 "https://faalkaart.test/proxy/https://api.tiles.mapbox.com/v4/mapbox.light/8/132/84.png"') do
  its(:stderr) {should match ('HTTP/1.1 200 OK')}
end

describe command('curl -sSv http://faalkaart.test') do
  # should redirect to https
  its(:stderr) {should match ('HTTP/1.1 301')}
  its(:stderr) {should match ('Location: https://faalkaart.test')}
end

describe command('curl -sSvk --http1.1 https://www.faalkaart.test') do
  # should redirect www to no-www
  its(:stderr) {should match ('HTTP/1.1 301')}
  its(:stderr) {should match ('Location: https://faalkaart.test')}
end

# stats have explicit cache which is different from the webserver 10 minute default
# implicitly tests database migrations as it will return 500 if they are not applied
describe command('curl -sSvk --http1.1 "https://faalkaart.test/data/terrible_urls/NL/municipality/0"') do
  # should redirect www to no-www
  its(:stderr) {should match ('Cache-Control: max-age=86400')}
end

# all responses should be compressed
# proxied html
describe command('curl --compressed -sSvk --http1.1 "https://faalkaart.test/"') do
  its(:stderr) {should match ('Content-Encoding: gzip')}
end
# proxied JSON
describe command('curl --compressed -sSvk --http1.1 "https://faalkaart.test/data/stats/NL/municipality/0"') do
  its(:stderr) {should match ('Content-Encoding: gzip')}
end
# proxied static files
describe command('curl --compressed -sSvk --http1.1 "https://faalkaart.test/static/images/internet_cleanup_foundation_logo.png"') do
  its(:stderr) {should match ('Content-Encoding: gzip')}
end

# HTTP2 request to frontend
# debian 8 curl does not support http2
curl_http2_support = !bash('curl --version').stdout.match(/--http2/).blank?
curl_cmd = curl_http2_support ? "curl" : "docker run --network=host getourneau/alpine-curl-http2 curl"

describe command("#{curl_cmd} -sSvk --http2 https://faalkaart.test") do
  # should return successful
  its(:stderr) {should match ('HTTP/2 200')}
  # render complete page
  its(:stdout) {should match ('MSPAINT')}
  # HSTS security should be enabled
  its(:stderr) {should match ('strict-transport-security: max-age=31536000; includeSubdomains')}
end

describe command('curl -sSvk --http1.1 https://faalkaart.test/authentication/login/') do
  # should return successful
  its(:stderr) {should match ('HTTP/1.1 200 OK')}
end

# multiple requests in quick succession to login
describe bash('for i in 1..4; do curl -sSvk --http1.1 https://faalkaart.test/authentication/login/ &>/dev/null & done;wait;curl -sSvk --http1.1 https://faalkaart.test/authentication/login/') do
  # should end up being rate limited
  its(:stderr) {should match ('HTTP/1.1 503 Service Temporarily Unavailable')}
end

# unauthenticated request to game
describe command('curl -sSvk --http1.1 https://faalkaart.test/game/') do
  # should be disallowed
  its(:stderr) {should match ('HTTP/1.1 404 Not Found')}
end

# Disabled?
# authenticated request to game
describe command('curl -sSvk --http1.1 --cookie "sessionid=123" https://faalkaart.test/game/') do
  # should be allowed
  its(:stderr) {should match ('HTTP/1.1 200 OK')}
end



